/*
*   TODO: write description
* */
Organizer.toDoDialog = {
    currentDialogId : null,

    dialogType : {
        '#editPage' : 'edit',
        '#deletePage' : 'delete'
    },

    init : function( params ){
        var instance = this,
            deletePageDelete = jQuery( params.deletePageDelete ),
            deletePageCancel = jQuery( params.deletePageCancel ),
            editPageSave = jQuery( params.editPageSave ),
            editPageCancel = jQuery( params.editPageCancel );

        deletePageDelete.click(function(){
            console.info('DELETE');
            Organizer.toDo.removeItem();
        });

        editPageSave.click(function(){
            console.info('SAVE');
            Organizer.toDo.saveAfterEdit();
        });
    },

    initDialogs: function( item ){
        var instance = this,
            itemId = item.id,
            itemElement = jQuery('#itemId_' + itemId );



        itemElement.find('.showDialog').click(function( e ){
            var buttonHref = jQuery( e.currentTarget ).attr('href');
            instance.currentDialogId = itemId;

            console.log( instance.dialogType[ buttonHref ] );

            switch( instance.dialogType[ buttonHref ] ){
                case 'edit':
                    Organizer.toDo.setDataToEditForm();
                break;
                default:
                    // do nothing
                break;
            }

            console.log( 'CLICK MAN id=' + itemId );
        });
    }

};
