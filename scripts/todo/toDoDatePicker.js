/*
*
* */
Organizer.toDoDatePicker = {
    init : function( params ){
        var instance = this,
            addPage = jQuery( params.addPage ),
            editPage = jQuery( params.editPage );

        instance.toDoDate = jQuery( params.toDoDate );
        instance.toDoDateEdit = jQuery( params.toDoDateEdit );

        addPage.live( 'pageinit', function(event){
            instance._initDatePicker( instance.toDoDate );
        });

        editPage.live( 'pageinit', function(event){
            instance._initDatePicker( instance.toDoDateEdit );
        });
    },

    _initDatePicker : function( dateElement ){
        var instance = this,
            currentDate = moment( new Date() );
//            defaultPickerValue = [ currentDate.year(), currentDate.month() , currentDate.day() ],
//            presetDate = new Date(defaultPickerValue[0], defaultPickerValue[1], defaultPickerValue[2], 0, 0, 0, 0),
//            todaysDate = new Date(),
//            lengthOfDay = 24 * 60 * 60 * 1000,
//            diff = parseInt((((presetDate.getTime() - todaysDate.getTime()) / lengthOfDay)+1)*-1,10),
//
//            minDay = currentDate.day() - 1,
//            minDate = defaultPickerValue = [ currentDate.year(), currentDate.month() , minDay ];

        //console.log( defaultPickerValue );
        //console.log( diff );
        //dateElement.data('datebox').options.defaultPickerValue = defaultPickerValue;
        //dateElement.data('datebox').options.minDays = 12;//diff;

        //console.log( dateElement.data('datebox').options );

        console.info(' DatePicker init ');
    }
};
