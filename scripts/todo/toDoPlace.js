Organizer.toDoPlace = {
    /*
    *   Take this sources from the internet
    * */
    CURRENT_MARKER_IMAGE : 'http://maps.google.com/mapfiles/arrow.png',
    DEFAULT_MARKER_IMAGE : 'http://maps.google.com/mapfiles/marker.png',

    init : function( params ){
        var instance = this;

        instance.place = jQuery( params.place );
        instance.map_canvas = jQuery( params.map_canvas );

        // set map height
        //instance.map_canvas.height( instance._getMapHeight() );
        //instance.animation = new google.maps.Animation();



        instance.place.live( 'pageinit',function(){
            instance._renderMap();
        });
    },

    /*
    *   Find out map height
    * */
    _getMapHeight : function(){
        var instance = this,
            document = jQuery(document),
            header = jQuery('.ui-header'),
            mapHeight = document.height() - 2 * header.height();

        console.log( jQuery(document) );
        // document - header - footer
        console.log( mapHeight );
        return mapHeight;
    },

    _renderMap : function(){
        var instance = this;

        // get current position
        navigator.geolocation.getCurrentPosition( successCallback, errorCallback, {maximumAge:600000} );

        function successCallback(position) {
            //console.log( position );
            // By using the 'maximumAge' option above, the position
            // object is guaranteed to be at most 10 minutes old.
            instance.latitude = position.coords.latitude;
            instance.longitude = position.coords.longitude;

            //console.log( instance.latitude + '  ' + instance.longitude );

            var myOptions = {
                center: new google.maps.LatLng( instance.latitude, instance.longitude ),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            instance.map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            instance.showCurrentMarker();

            google.maps.event.addListener( instance.map, 'click', function(event) {
                //console.log( event.latLng );
                instance.placeMarker( event.latLng);
            });




            console.log('RENDER MAP');
        }

        function errorCallback(error) {
            // Update a div element with error.message.
            alert('error to get current position!');
        }
    },

    /*
    *
    * */
    _clickMarkerFunctional : function(  ){
        var instance = this;

        alert('click');
    },

    _initMarkerEvents : function( marker ){
        var instance = this;

//        google.maps.event.addListener( marker, 'click', instance.toggleBounce( marker ) );
//
        google.maps.event.addListener( marker, 'mov', instance._dragStart() );
        google.maps.event.addListener( marker, 'dragEnd', instance._dragEnd() );
//
//        google.maps.event.addListener( marker, '', instance._dragEnd() );

        //google.maps.event.addListener( marker, 'doubleClick', instance._clickMarkerFunctional() );
        // add marker event
        //GEvent.addListener( marker, 'click', instance._clickMarkerFunctional() );
        //marker.addEventListener( 'click', instance._clickMarkerFunctional() );
    },

    _dragStart : function(){
        console.log( 'start' );
    },

    _dragEnd : function(){
        console.log( 'end' );
    },

    /*
    *
    * */
    _getMarkerIcon : function( currentMarker ){
        var instance = this,
            image;

        if( currentMarker ){
            image = instance.CURRENT_MARKER_IMAGE;
        } else {
            image = instance.DEFAULT_MARKER_IMAGE;
        }

        return new google.maps.MarkerImage( image );
    },

    /*
    *
    * */
    showCurrentMarker : function(){
        var instance = this,
            currentLatLng = new google.maps.LatLng( instance.latitude, instance.longitude ),
            currentMarker = true;
        //console.log( currentLatLng );
        instance.placeMarker( currentLatLng, currentMarker  );
    },

    /*
    *
    * */
    placeMarker : function( location, currentMarker ){
        var instance = this,
            isDraggableMarker = currentMarker ? false : true,
                //http://maps.google.com/mapfiles/arrow.png
            marker = new google.maps.Marker({
                //animation : instance.animation.BOUNCE,
                position : location,
                icon : instance._getMarkerIcon( currentMarker ),
                draggable: isDraggableMarker,
                animation: google.maps.Animation.DROP,
                map : instance.map,
                title : 'some text'
            });
        //instance.map.setCenter( location );
        instance._initMarkerEvents( marker );
    },

    /*
    *
    * */
    toggleBounce : function( marker ){
        var instance = this;
        if (marker.getAnimation() != null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
            console.log('end');
        }
    }

 };


