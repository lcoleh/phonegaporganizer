/*
*
*
*
* */
Organizer.ToDoModel = function() {
    var items = [],
        load = function() {
            items = JSON.parse($.cookie("toDoModel") || "[]");
        },
        currentId = 0;


    this.PRIORITY = {
        IMPORTANT: 2,
        NORMAL: 1,
        LOW: 0
    };

    this.save = function() {
        $.cookie("toDoModel", JSON.stringify(items), { expires: 365 });
    };

    this.create = function(text, priority, date) {
        var result = {
            "id" : _generateId(),
            "text": text,
            "priority": priority,
            "date" : date
        };

        items.push(result);
        this.save();

        return result;
    };

    this.remove = function(item) {
        items.splice($(items).index(item), 1);
        this.save();
    };

    this.clear = function() {
        items = [];
        this.save();
    };

    this.getItems = function() {
        var result = [];

        for (var index = 0, max = items.length; index != max; index++) {
            result.push(items[index])
        }

        return result;
    };

    this.getItemById = function( id ){
        var returnedItem = false;
        for( var i = 0 ; i < items.length ; i += 1 ){
            var item = items[ i ];
            if( item.id == id ){
                returnedItem = item;
            }
        }
        return returnedItem;
    };

    function _getLastItemId(){
        var itemsLength = items.length,
            maxId = 1;
        if( itemsLength ){
            for( var i = 0 ; i < itemsLength ; i += 1 ){
                var item = items[i];
                if( item.id > maxId ){
                    maxId = item.id;
                }
            }
            return maxId;
        } else {
            return false;
        }
    }

    function _generateId(){
        var lastItemId = _getLastItemId();
        if( lastItemId ){
            console.log( "last + 1 " + parseInt( lastItemId + 1 ) );
            return lastItemId + 1;
        } else {
            console.log( 'current' );
            return currentId++;
        }

    }

    load();
    //console.info('items:');
    //console.log(items);
};