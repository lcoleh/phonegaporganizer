Organizer.toDo = {
    init : function( params ){
        var instance = this,
            createButton = jQuery( params.createButton );

        instance.toDoList = jQuery( params.toDoList );

        // add new
        instance.toDoUrgency = jQuery( params.toDoUrgency );
        instance.toDoDescription = jQuery( params.toDoDescription );

        // edit
        instance.toDoUrgencyEdit = jQuery( params.toDoUrgencyEdit );
        instance.toDoDescriptionEdit = jQuery( params.toDoDescriptionEdit );

        instance.toDoImportant = jQuery( params.toDoImportant );
        instance.toDoToday = jQuery( params.toDoToday );

        instance.listHeaders = [];
        instance.model = new Organizer.ToDoModel();

        console.log( instance.model.getItems() );

        // CLEAR THE MODEL
        //instance.model.clear();

        createButton.click(function(){
            instance._createNew();
        });

        instance._getListHeaders();
        instance._renderExistingItems();
        instance.renderTodayItems();
        instance.renderImportantItems();

    },

    /*
    *   Private
    * */
    _getListHeaders : function(){
        var instance = this;
        instance.toDoList.find('li[data-priority]').each(function(){
            var item = jQuery(this);
            instance.listHeaders[parseInt( item.attr('data-priority') )] = item;
        });
    },

    _renderItemElement : function( item ){
        var instance = this;
        return jQuery.tmpl("<li id='itemId_${id}' data-icon=\"delete\" class=\"item\">" +
            "<fieldset class='ui-grid-a'>" +
                "<div class='ui-block-a'><div class='text'>${text}</div><div class='date'>${date}</div></div>" +
                "<div class='ui-block-b'>" +
                    "<fieldset class='ui-grid-a'>" +
                        "<div class='ui-block-a'><a href='#editPage' data-icon='gear' data-role='button' data-rel='dialog' class='showDialog'></a></div>" +
                        "<div class='ui-block-b'><a href='#deletePage' data-theme='b' data-rel='dialog' data-icon='minus' data-role='button' class='showDialog'></a></div>" +
                    "</fieldset>" +
                "</div>" +
            "</fieldset>"

            + "</li>", item )
            .data("item", item)
            .insertAfter(instance.listHeaders[item.priority]);
    },

    _renderExistingItems : function(){
        var instance = this;
        jQuery( instance.model.getItems() ).each(function(){
            instance._renderItemElement(this);

            // init dialog
            Organizer.toDoDialog.initDialogs( this );
        });
    },

    _createNew : function(){
        var instance = this,
            priority = parseInt( instance.toDoUrgency.val() ),
            date = Organizer.toDoDatePicker.toDoDate.val(),
            item = instance.model.create( instance.toDoDescription.val(), priority, date );

        instance.toDoUrgency.val( instance.model.PRIORITY.NORMAL.toString() ).trigger("change");
        instance.toDoDescription.val("");

        // render item
        instance._renderItemElement( item );
        Organizer.toDoDialog.initDialogs( item );
        // refresh list and buttons
        instance.toDoList.trigger('create');
        instance.toDoList.listview('refresh');
        instance._updateWidgets();
    },

    /*
    *   TODO: rewrite this
    * */
    _removeItemsHack : function(){
        var instance = this;

        instance.toDoList.find('li.item').each(function(){
            jQuery(this).remove();
        });
    },

    renderImportantItems : function(){
        var instance = this,
            items = instance.model.getItems();

        if( instance._hasImportantItems() && Organizer.model.getImportantEvents() ){
            var content = '<ul data-role="listview"data-inset="true">' +
                '<li class="red" data-role="list-divider">This is important</li>';

            for( i = 0 ; i < items.length ; i+= 1 ){
                var item = items[i];
                if( item.priority == 2 ){
                    content += '<li >' + item.text + '</li>';//data-icon="false"
                }
            }

            content += '</ul>';
            instance.toDoImportant.html( content );

            instance.toDoImportant.find('ul').listview();
        }else{
            instance.toDoImportant.html('');
        }
    },

    renderTodayItems : function(){
        var instance = this,
            items = instance.model.getItems(),
            today = moment( new Date() ).format("YYYY-MM-DD"),
            countToday = 0;

        if( instance._hasImportantItems() && Organizer.model.getTodayEvents() ){
            var content = '<ul data-role="listview"data-inset="true">' +
                '<li class="red" data-role="list-divider">For today( ' + today + ' )</li>';

            for( i = 0 ; i < items.length ; i+= 1 ){
                var item = items[i];
                if( item.date == today ){
                    countToday++;
                    content += '<li >' + item.text + '</li>';//data-icon="false"
                }
            }

            content += '</ul>';

            if( countToday > 0 ){
                instance.toDoToday.html( content );
                instance.toDoToday.find('ul').listview();
            } else {
                instance.toDoToday.html('');
            }
        }else{
            instance.toDoToday.html('');
        }
    },

    _hasImportantItems : function(){
        var instance = this,
            items = instance.model.getItems();

        for( var i = 0 ; i < items.length ; i += 1 ){
            var item = items[i];
            if( item.priority == 2 ){
                return true;
            }
        }
        return false;
    },

    /*
    *   TODO : rewrite this hack
    *   Rewrite
    *       - today items
    *       - important items
    * */
    _updateWidgets : function(){
        var instance = this;
        // change visuals on todayList
        // TODO: rewrite this hack
        instance.renderTodayItems();

        // change visuals on importantList
        // TODO: rewrite this hack
        instance.renderImportantItems();
    },

    /*
    *   Public
    * */
    setDataToEditForm : function(){
        var instance = this,
            currentItem = instance.model.getItemById( Organizer.toDoDialog.currentDialogId );

        console.log( currentItem );

        // set data from item to edit form
        instance.toDoDescriptionEdit.val( currentItem.text );
        Organizer.toDoDatePicker.toDoDateEdit.val( currentItem.date );
        instance.toDoUrgencyEdit.val( currentItem.priority );

    },

    removeItem : function(){
        var instance = this,
            item = jQuery('#itemId_' + Organizer.toDoDialog.currentDialogId );

        instance.model.remove( item.data("item") );
        item.slideUp(function() {
            item.remove();

            instance._updateWidgets();
        });
    },

    saveAfterEdit : function(){
        var instance = this,
            item = jQuery('#itemId_' + Organizer.toDoDialog.currentDialogId ),
            itemInModel = instance.model.getItemById( Organizer.toDoDialog.currentDialogId ),

            newText = instance.toDoDescriptionEdit.val(),
            newDate = Organizer.toDoDatePicker.toDoDateEdit.val(),

            oldPriority = itemInModel.priority,
            newPriority = parseInt( instance.toDoUrgencyEdit.val() ),

            tempItemInModel = itemInModel;

        // if we change nothing
        if( itemInModel.text == newText &&
            itemInModel.date == newDate &&
            itemInModel.priority == newPriority )
        {
            return false;
        }

        // change visuals on toDoList
        itemInModel.text = newText;
        itemInModel.date = newDate;
        itemInModel.priority = newPriority;
        instance.model.save();

        // update toDoList
        if( oldPriority != newPriority ){ // if priority change
            item.remove();
            instance._renderItemElement( tempItemInModel );
            // init dialog functional
            Organizer.toDoDialog.initDialogs( item );
            // refresh list and buttons
            instance.toDoList.trigger('create');
            instance.toDoList.listview('refresh');
        } else { // just change the text
            item.find('.text').html( newText );
            item.find('.date').html( newDate );
        }

        instance._updateWidgets();
    }
};
