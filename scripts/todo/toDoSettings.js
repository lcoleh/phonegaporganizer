/**
*   Organizer settings
 */
Organizer.toDoSettings = {
    init : function( params ){
        var instance = this;

        instance.settings = jQuery( params.settings );
        instance.todayCheckbox = jQuery( params.todayCheckbox );
        instance.importantCheckbox = jQuery( params.importantCheckbox );

        /*
        *   Show / hide today
        * */
        instance.todayCheckbox.change(function(e){
            var checked = jQuery( e.currentTarget ).is(':checked');
            Organizer.model.setTodayEvents( checked );
            Organizer.toDo.renderTodayItems();
//            if( checked ){
//                //Organizer.toDo.toDoToday.show();
//                Organizer.toDo.renderTodayItems();
//            } else {
//                //Organizer.toDo.toDoToday.hide();
//            }
        });

        /*
         *   Show / hide important
         * */
        instance.importantCheckbox.change(function(e){
            var checked = jQuery( e.currentTarget ).is(':checked');
            Organizer.model.setImportantEvents( checked );
            Organizer.toDo.renderImportantItems();
//            if( checked ){
//                Organizer.toDo.toDoImportant.show();
//            } else {
//                Organizer.toDo.toDoImportant.hide();
//            }
        });

        instance._initState();
    },

    /*
    *   Set values when we init the page
    *   Take info from db
    * */
    _initState : function(){
        var instance = this;

        instance.todayCheckbox.attr( 'checked', Organizer.model.getTodayEvents());
        instance.importantCheckbox.attr( 'checked', Organizer.model.getImportantEvents() );
    }

};
