Organizer.OrganizerModel = function(){
    var showTodayEvents,
        showImportantEvents;

    function load(){
        var cookieShowTodayEvents = $.cookie("showTodayEvents"),
            cookieShowImportantEvents = $.cookie("showImportantEvents");

        if (!cookieShowTodayEvents) {
            $.cookie("showTodayEvents", true, { expires:365 });
            showTodayEvents = true;
        } else {
            showTodayEvents = JSON.parse(cookieShowTodayEvents);
        }
        if (!cookieShowImportantEvents) {
            $.cookie("showImportantEvents", true, { expires:365 });
            showImportantEvents = true;
        } else {
            showImportantEvents = JSON.parse(cookieShowImportantEvents);
        }
    }

    /*
    *   Setters
    * */
    this.setTodayEvents = function( show ){
        showTodayEvents = show;
        $.cookie("showTodayEvents", JSON.stringify( show ), { expires: 365 });
    };

    this.setImportantEvents = function( show ){
        showImportantEvents = show;
        $.cookie("showImportantEvents", JSON.stringify( show ), { expires: 365 });
    };

    /*
    *   Getters
    * */
    this.getTodayEvents = function(){
        return showTodayEvents;
    };

    this.getImportantEvents = function(){
        return showImportantEvents;
    };

    /*
    *   Clear
    * */
    this.clearConfiguration = function(){
        $.cookie("showTodayEvents", '', { expires: 365 });
        $.cookie("showImportantEvents", '', { expires: 365 });
    };

    load();
};

