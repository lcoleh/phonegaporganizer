jQuery(document).ready(function(){
    /*
    *   Init main Organizer Model
    * */
    Organizer.model = new Organizer.OrganizerModel();
    //Organizer.model.clearConfiguration();

    /*
    *   Main object
    * */
    Organizer.toDo.init({
        toDoList : '#todoList',

        // add new form
        toDoUrgency : '#toDoUrgency',
        toDoDescription : '#toDoDescription',

        // edit form
        toDoUrgencyEdit : '#toDoUrgencyEdit',
        toDoDescriptionEdit : '#toDoDescriptionEdit',

        createButton : '#createButton',
        toDoImportant : '#toDoImportant',
        toDoToday : '#toDoToday'

    });

    /*
     *   Settings init
     * */
    Organizer.toDoSettings.init({
        settings : '#settings',
        todayCheckbox : '#todayCheckbox',
        importantCheckbox : '#importantCheckbox'
    });

    /*
    *   Dialog init
    * */
    Organizer.toDoDialog.init({
        'deletePageDelete' : '#deletePageDelete',
        'deletePageCancel' : '#deletePageCancel',
        'editPageSave' : '#editPageSave',
        'editPageCancel' : '#editPageCancel'
    });

    /*
    *   DatePicker init
    * */
    Organizer.toDoDatePicker.init({
        addPage : '#addPage',
        toDoDate : '#toDoDate',
        editPage : '#editPage',
        toDoDateEdit : '#toDoDateEdit'
    });

    /*
    *   Place init
    * */
    Organizer.toDoPlace.init({
        place : '#place',
        map_canvas : '#map_canvas'
    });
});
